<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OriginDestination extends Model
{
    protected $table = 'origin_destination';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id_origin',
        'id_destination'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function origin()
    {
        return $this->hasOne('App\Place', 'id', 'id_origin');
    }

    public function destination()
    {
        return $this->hasOne('App\Place', 'id', 'id_destination');
    }

}
