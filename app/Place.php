<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table = 'place';

    protected $primaryKey = 'id';

    protected $fillable = [
        'key',
        'name'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function destinations()
    {
        return $this->belongsToMany('App\Place', 'origin_destination', 'id_origin', 'id_destination');
    }

}
