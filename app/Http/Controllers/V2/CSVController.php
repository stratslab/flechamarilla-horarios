<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use stdClass;

class CSVController extends Controller
{
    public function getHorarios($clave_lugar, $clave_pantalla)
    {
        $nombre_archivo = $clave_lugar . '.csv';

        if (file_exists(public_path($nombre_archivo))) {
            //Recorre el archivo csv con el nombre de la clave del lugar y lo transforma en una colección
            $destinations = (new FastExcel)->import($nombre_archivo, function ($line) {
                //Toma el valor de la posición "Destino"
                $name = $line['Destino'];
                //Toma el valor de la posición "Tarifa"
                $amount = $line['Tarifa'];
                //Separa el valor de la posición "Horarios" para ponerlo como un arreglo
                $horarios = explode(',', $line['Horarios']);

                //Crea un objeto y asigna los valores anteriormente obtenidos
                $destination = new stdClass();
                $destination->name = $name;
                $destination->amount = $amount;
                $destination->horarios = $horarios;

                return $destination;
            });

            return view('v2.horarios')
                ->with('destinations', $destinations)
                ->with('clave_lugar', $clave_lugar)
                ->with('clave_pantalla', $clave_pantalla)
                ->with('type', 'csv');
        } else {
            return view('v2.error')
                ->with('error', 'Ops!, parece que no existe un archivo csv con la clave ingresada: ' . $clave_lugar . '...');
        }
    }
}
