<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use stdClass;
use Storage;

class CSVController extends Controller
{
    public function coordinadosMex(Request $request)
    {
        $screen = $request->input('screen');

        $destinations = (new FastExcel)->import('coordinados_mex_alternative.csv', function ($line) {
            $name = $line['Destino'];
            $amount = $line['Tarifa'];
            $horarios = explode(',', $line['Horarios']);

            $destination = new stdClass();
            $destination->name = $name;
            $destination->amount = $amount;
            $destination->horarios = $horarios;

            return $destination;
        });

        return view('horarioscsv')
            ->with('destinations', $destinations)
            ->with('screen', $screen);
    }
}
