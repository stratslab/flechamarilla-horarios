<?php

namespace App\Http\Controllers;

use App\Origin;
use App\Place;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use SoapClient;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //Cliente para generar peticiones SOAP
    protected $soap_client;

    protected $GFA_USER;

    protected $GFA_PASSWORD;

    protected $GFA_SESSIONID;

    protected $GFA_URL;

    /**
     * Constructor para inicializar todas las variables globables
     * @return null
     */
    /*public function __construct()
    {
        $this->GFA_USER = env('GFA_USER');
        $this->GFA_PASSWORD = env('GFA_PASSWORD');
        $this->GFA_SESSIONID = env('GFA_SESSIONID');
        $this->GFA_URL = env('GFA_URL');

        $this->soap_client = new SoapClient($this->GFA_URL, [
            'encoding' => 'UTF-8',
            'trace' => true
        ]);
    }

    public function login()
    {
        $data = [
            'MexicaUser' => $this->GFA_USER,
            'MexicaPassWord' => $this->GFA_PASSWORD,
            'Session_Id' => $this->GFA_SESSIONID
        ];

        $response = $this->soap_client->__soapCall('Login', $data);
    }

    public function getHorariosRemoto(Request $request)
    {
        $this->login();

        $key = $request->input('key');
        $screen = $request->input('screen');

        if (!is_null($key)) {
            //Ponemos en letras capital la clave.
            $key = strtoupper($key);

            //Buscamos el lugar apartir de la clave ingresada en el request
            $place = Place::where('key', '=', $key)->first();

            if (!is_null($place)) {
                //Destinos a los que puede llegar desde el origen
                $destinations = $place->destinations;

                if (count($destinations) > 0) {
                    $hour = date('H:i'); //Hora actual
                    $days = $this->getDays() + 1; //Días que han pasado desde fecha de GFA

                    //Generamos una petición por cada uno de los destinos del origen
                    foreach ($destinations as $destination) {
                        $data = [
                            'Origen' => $key,
                            'Destino' => strtoupper($destination->key),
                            'Linea' => 'GFA',
                            'Clase' => 'P',
                            'Hora' => $hour,
                            'Fecha' => $days,
                            'Session_Id' => $this->GFA_SESSIONID
                        ];

                        //Generamos la llamada al endpoint a la funcion "GetHorarios".
                        $response = $this->soap_client->__soapCall('GetHorarios', $data);

                        //Asignamos como parámetro la lista de horarios al destino.
                        $destination->horarios = $response['Horarios'];

                        //Si tiene horarios, asignamos el precio del boleto.
                        if (count($destination->horarios) > 0) {
                            $destination->amount = $destination->horarios[0]->Tarifas[0]->Total;
                        }
                    }


                    return view('horarios')
                        ->with('place', $place)
                        ->with('destinations', $destinations)
                        ->with('screen', $screen);
                } else {
                    //TODO return view que no se encuentran destinos para el lugar
                }
            } else {
                //TODO return view que no se encuentra lugar con la clave ingresada
            }
        } else {
            //TODO return view de que no has ingresado origen
        }
    }*/

    /**
     * Calcula los días que han pasado desde la fecha asignada de GFA (31/12/1899)
     * al día actual.
     * @return Int: Número de días transcurridos de una fecha a otra.
     */
    private function getDays()
    {
        $now = time();
        $your_date = strtotime("1899-12-31");
        $datediff = $now - $your_date;
        $days = $datediff / (60 * 60 * 24);

        return $days;
    }
}
