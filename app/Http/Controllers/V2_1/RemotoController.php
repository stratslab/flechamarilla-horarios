<?php

namespace App\Http\Controllers\V2_1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SoapClient;
use App\Place;

class RemotoController extends Controller
{
    //Cliente para generar peticiones SOAP
    protected $soap_client;

    //Endpoint de GFA donde se hacen las peticiones
    protected $GFA_URL;

    //Usuario de GFA para hacer llamadas
    protected $GFA_USER;

    //Contraseña del usuario de GFA
    protected $GFA_PASSWORD;

    //ID de la sesión que realizará las peticiones
    protected $GFA_SESSIONID;

    /**
     * Constructor para inicializar todas las variables globables
     * @return null
     */
    public function __construct()
    {
        $this->GFA_USER = env('GFA_USER');
        $this->GFA_PASSWORD = env('GFA_PASSWORD');
        $this->GFA_SESSIONID = env('GFA_SESSIONID');
        $this->GFA_URL = env('GFA_URL');

        $this->soap_client = new SoapClient($this->GFA_URL, [
            'encoding' => 'UTF-8',
            'trace' => true
        ]);
    }

    /**
     * Genera un inicio de sesión en la plataforma de GFA
     */
    public function login()
    {
        $data = [
            'MexicaUser' => $this->GFA_USER,
            'MexicaPassWord' => $this->GFA_PASSWORD,
            'Session_Id' => $this->GFA_SESSIONID
        ];

        $response = $this->soap_client->__soapCall('Login', $data);
    }

    /**
     * Obtiene los horarios de un origen remotamente, utilizando el servicio
     * proporcionado por GFA.
     */
    public function getHorarios($clave_lugar, $clave_pantalla)
    {
        $this->login();

        //Ponemos en letras capital la clave.
        $key = strtoupper($clave_lugar);

        //Buscamos el lugar apartir de la clave ingresada en el request
        $place = Place::where('key', '=', $key)->first();

        if (!is_null($place)) {
            //Destinos a los que puede llegar desde el origen
            $destinations = $place->destinations;

            if (count($destinations) > 0) {
                $hour = date('H:i'); //Hora actual
                $days = $this->getDays() + 1; //Días que han pasado desde fecha de GFA

                //Generamos una petición por cada uno de los destinos del origen
                foreach ($destinations as $destination) {
                    $data = [
                        'Origen' => $key,
                        'Destino' => strtoupper($destination->key),
                        'Linea' => 'GFA',
                        'Clase' => 'P',
                        'Hora' => $hour,
                        'Fecha' => $days,
                        'Session_Id' => $this->GFA_SESSIONID
                    ];

                    //Generamos la llamada al endpoint a la funcion "GetHorarios".
                    $response = $this->soap_client->__soapCall('GetHorarios', $data);

                    //Arreglo de horarios a ser ingresados
                    $horarios = [];

                    //Por cada uno de los horarios obtenidos, obtiene la hora de salida y lo ingresa al arreglo
                    foreach ($response['Horarios'] as $horario) {
                        array_push($horarios, $horario->HoraSale->Hora);
                    }

                    //Asignamos como parámetro la lista de horarios al destino.
                    $destination->horarios = $horarios;

                    //Si tiene horarios, asignamos el precio del boleto.
                    if (count($response['Horarios']) > 0) {
                        $destination->amount = $response['Horarios'][0]->Tarifas[0]->Total;
                    }
                }

                return view('v2_1.horarios')
                    ->with('place', $place)
                    ->with('destinations', $destinations)
                    ->with('clave_lugar', $clave_lugar)
                    ->with('clave_pantalla', $clave_pantalla)
                    ->with('type', 'remoto');
            } else {
                return view('v2_1.error')
                    ->with('error', 'Ops!, parece que el lugar ' . $clave_lugar . ' no tiene ningún destino asignado...');
            }
        } else {
            return view('v2_1.error')
                ->with('error', 'Ops!, parece que no existe un lugar con la clave ingresada: ' . $clave_lugar . '...');
        }
    }

    /**
     * Calcula los días que han pasado desde la fecha asignada de GFA (31/12/1899)
     * al día actual.
     * @return Int: Número de días transcurridos de una fecha a otra.
     */
    private function getDays()
    {
        $now = time();
        $your_date = strtotime("1899-12-31");
        $datediff = $now - $your_date;
        $days = $datediff / (60 * 60 * 24);

        return $days;
    }
}
