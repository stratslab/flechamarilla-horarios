<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Versión 1
Route::get('/', 'Controller@login');

Route::get('gethorarios', 'Controller@getHorariosRemoto');

Route::group(['prefix' => 'csv'], function() {
    Route::get('coordinadosmex', 'CSVController@coordinadosMex');
});

//Versión 2
Route::group(['prefix' => 'v2'], function() {
    Route::group(['prefix' => 'horarios'], function() {
        Route::get('remoto/{clave_lugar}/{clave_pantalla}', 'V2\RemotoController@getHorarios');
        Route::get('csv/{clave_lugar}/{clave_pantalla}', 'V2\CSVController@getHorarios');
    });
    Route::group(['prefix' => 'utils'], function () {
        Route::get('checkconnection', 'V2\ConnectionController@checkConnection');
    });
});


//Versión 2.1
Route::group(['prefix' => 'v2_1'], function() {
    Route::group(['prefix' => 'horarios'], function() {
        Route::get('remoto/{clave_lugar}/{clave_pantalla}', 'V2_1\RemotoController@getHorarios');
        Route::get('csv/{clave_lugar}/{clave_pantalla}', 'V2_1\CSVController@getHorarios');
    });
    Route::group(['prefix' => 'utils'], function () {
        Route::get('checkconnection', 'V2_1\ConnectionController@checkConnection');
    });
});
