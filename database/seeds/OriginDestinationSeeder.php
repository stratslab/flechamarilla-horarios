<?php

use App\Place;
use App\OriginDestination;
use App\Schedule;
use Illuminate\Database\Seeder;

class OriginDestinationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (file(public_path('origindestination.txt')) as $origin_destinations) {
            $data = explode('-', $origin_destinations);
            $key_origin = $data[0];
            $data_keys_destinations = $data[1];

            $keys_destinations = explode('/', $data_keys_destinations);

            $origin = Place::where('key', '=', $key_origin)->first();

            foreach ($keys_destinations as $key_destination) {
                $destination = Place::where('key', '=', $key_destination)->first();

                if (!is_null($destination)) {
                    OriginDestination::create([
                        'id_origin' => $origin->id,
                        'id_destination' => $destination->id
                    ]);
                }
            }
        }
    }
}
