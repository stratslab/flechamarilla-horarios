<?php

use App\Place;
use Illuminate\Database\Seeder;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (file(public_path('places.txt')) as $place) {
            $data = explode('-', $place);

            Place::create([
                'key' => $data[0],
                'name' => $data[1]
            ]);
        }
    }
}
