<!DOCTYPE html>
<html>

<head>
    <!-- Título: GFA - (Tipo de horarios), (Clave de lugar), (Clave de pantalla) -->
    <title>GFA - {{ $type }}, {{ $clave_lugar }}, {{ $clave_pantalla }}</title>

    <!-- Compatibilidad con dispositivos móviles -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Scripts -->
    <!-- JQuery -->
    <script src="{{ url('js/jquery.js') }}"></script>
    <!-- Popper -->
    <script src="{{ url('js/popper.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ url('js/bootstrap.js') }}"></script>
    <!-- Firebase -->
    <script src="https://www.gstatic.com/firebasejs/5.10.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.10.1/firebase-database.js"></script>
    <!-- Propios -->
    <script src="{{ url('js/v2.1/horarios.js') }}"></script>

    <!-- CSS -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
    <!-- Propios -->
    <link rel="stylesheet" href="{{ url('css/v2/horarios.css') }}">

    <style>
        .background-transparent {
            background-color: transparent !important;
        }
    </style>
</head>

<body style="overflow-x: hidden;">
    <?php
    $index = 0; //Determina si está en lugar par/impar, para poner un color diferente;
    ?>

    <!-- URL base para las peticiones AJAX -->
    <input type="hidden" id="url" value="{{ url('') }}">

    <!-- Claves con las cuales será consumido el servicio de Firebase (clave_lugar/clave_pantalla) -->
    <input type="hidden" id="clave_lugar" value="{{ $clave_lugar }}">
    <input type="hidden" id="clave_pantalla" value="{{ $clave_pantalla }}">

    <div id="background-container"
        style="width: 100vw; height: 100vh; top:0; bottom: 0; position: fixed; display: flex; align-items: center; justify-content: center; z-index: -9999;">
        <img src="{{ url('v2_1/img/logo-primera-plus.png') }}" style="width: 20vw" id="img-logo">
    </div>

    <div class="row text-center"
        style="background-color: #DA2927; color: white; font-weight: bold; padding: 5px 0px 5px 0px; position: fixed; top: 0; z-index: 9999; width: 105vw"
        id="header-container">
        <div class="col-2" id="header-destino">DESTINO</div>
        <div class="col-1" id="header-precio">TARIFA</div>
        <div class="col-9" id="header-horarios">HORARIOS</div>
    </div>

    <div id="horarios-container">
        <div id="horarios">
            <!-- Genera un renglón para cada destino -->
            @foreach ($destinations as $destination)
                <!-- El destino se muestra solo en caso de que tenga un horario o más -->
                @if (count($destination->horarios) > 0)
                    <!-- Dependiendo si está en índice par/impar muestra un color u otro -->
                    <div class="row {{ is_int($index / 2) ? 'destination-2' : 'destination' }}">
                        <!-- Nombre del destino -->
                        <div class="col-2 text-center no-padding">
                            <div class="place-name">
                                <p class="p-destino">
                                    {{ $destination->name }}
                                </p>
                            </div>
                        </div>
                        <!-- Precio del destino -->
                        <div class="col-1 no-padding">
                            <div class="place-price">
                                <p class="p-precio">
                                    $ {{ number_format((int) $destination->amount) }}
                                </p>
                            </div>
                        </div>
                        <!-- Horarios -->
                        <div class="col-9 no-padding">
                            <div class="place-horarios">
                                <p class="p-horarios">
                                    @foreach ($destination->horarios as $horario)
                                        <span class="destination-hour">{{ $horario }}</span>
                                    @endforeach
                                </p>
                            </div>
                        </div>
                        <!-- Aumentamos el índice para que el siguiente renglón esté de diferente color -->
                        <?php $index++; ?>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</body>

</html>
