<!DOCTYPE html>
<html>

<head>
    <title>Error</title>
    <!-- Compatibilidad con dispositivos móviles -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Scripts -->
    <!-- JQuery -->
    <script src="{{ url('js/jquery.js') }}"></script>
    <!-- Popper -->
    <script src="{{ url('js/popper.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ url('js/bootstrap.js') }}"></script>

    <!-- CSS -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
    <!-- Propios -->
    <link rel="stylesheet" href="{{ url('css/v2/error.css') }}">
</head>

<body>
    <div class="container-error">
        <img class="image-error" src="{{ url('img/error.png') }}">
        <p class="message-error">{{ $error }}</p>
    </div>
</body>

</html>