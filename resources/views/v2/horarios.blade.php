<!DOCTYPE html>
<html>

<head>
    <!-- Título: GFA - (Tipo de horarios), (Clave de lugar), (Clave de pantalla) -->
    <title>GFA - {{ $type }}, {{ $clave_lugar }}, {{ $clave_pantalla }}</title>

    <!-- Compatibilidad con dispositivos móviles -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Scripts -->
    <!-- JQuery -->
    <script src="{{ url('js/jquery.js') }}"></script>
    <!-- Popper -->
    <script src="{{ url('js/popper.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ url('js/bootstrap.js') }}"></script>
    <!-- Firebase -->
    <script src="https://www.gstatic.com/firebasejs/5.10.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.10.1/firebase-database.js"></script>
    <!-- Propios -->
    <script src="{{ url('js/v2/horarios.js') }}"></script>

    <!-- CSS -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
    <!-- Propios -->
    <link rel="stylesheet" href="{{ url('css/v2/horarios.css') }}">


</head>

<body>
    <?php
    $index = 0 //Determina si está en lugar par/impar, para poner un color diferente;
    ?>

    <!-- URL base para las peticiones AJAX -->
    <input type="hidden" id="url" value="{{ url('') }}">

    <!-- Claves con las cuales será consumido el servicio de Firebase (clave_lugar/clave_pantalla) -->
    <input type="hidden" id="clave_lugar" value="{{ $clave_lugar }}">
    <input type="hidden" id="clave_pantalla" value="{{ $clave_pantalla }}">

    <!-- Horarios -->
    <div id="horarios">
        <!-- Genera un renglón para cada destino -->
        @foreach($destinations as $destination)
        <!-- El destino se muestra solo en caso de que tenga un horario o más -->
        @if( count($destination->horarios) > 0)
        <!-- Dependiendo si está en índice par/impar muestra un color u otro -->
        <div class="row {{ ((is_int($index / 2))) ? 'destination-2' : 'destination'}}">
            <!-- Nombre del destino -->
            <div class="col-2 text-center no-padding">
                <div class="place-name">
                    <p class="p-destino">
                        {{ $destination->name }}
                    </p>
                </div>
            </div>
            <!-- Precio del destino -->
            <div class="col-1 no-padding">
                <div class="place-price">
                    <p class="p-precio">
                        $ {{ number_format((int) $destination->amount) }}
                    </p>
                </div>
            </div>
            <!-- Horarios -->
            <div class="col-9 no-padding">
                <div class="place-horarios">
                    <p class="p-horarios">
                        @foreach($destination->horarios as $horario)
                        <span class="destination-hour">{{ $horario }}</span>
                        @endforeach
                    </p>
                </div>
            </div>
            <!-- Aumentamos el índice para que el siguiente renglón esté de diferente color -->
            <?php $index++ ?>
        </div>
        @endif
        @endforeach
    </div>

</body>

</html>