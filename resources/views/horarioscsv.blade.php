<!DOCTYPE html>
<html>

<head>
    <title>Horarios Flecha Amarilla</title>

    <script src="{{ url('js/jquery.js') }}"></script>
    <script src="{{ url('js/popper.js') }}"></script>
    <script src="{{ url('js/bootstrap.js') }}"></script>
    <!--<script src="{{ url('js/horarios.js') }}"></script>-->
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/horarios_v2.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--<script src="https://www.gstatic.com/firebasejs/5.10.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.10.1/firebase-database.js"></script>
    <script src="{{ url('js/firebase.js') }}"></script>-->

    <script>
        $(document).ready(function() {
            let horarios = $('#horarios');

            //Hace el autoscroll cada 10 milisegundos y vuelve a llamar la función (Recursiva)
            function pageScroll() {
                window.scrollBy(0, 2);
                scrolldelay = setTimeout(pageScroll, 1);
            }

            //Genera la primera llamada a la función de autoscroll
            pageScroll();

            $(window).scroll(function() {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 10000) {
                    horarios.clone().appendTo('body');
                }
            });

        });
    </script>
</head>

<body>
    <input type="hidden" id="key" value="coordinados_mex">
    <input type="hidden" id="screen" value="{{ $screen }}">

    @if($screen == 'sala_1')
    <style>
        body {
            transform: scaleY(2.5) !important;
        }

        .place-name {
            background-color: rgb(23, 66, 14) !important;
        }

        .destination-2 {
            border-top-color: #007145 !important;
            background: rgba(39, 218, 96, 0.26);

        }

        .destination {
            border-top-color: #007145 !important;
        }

        p {
            font-style: normal !important;
            font-weight: bold !important;
            font-size: 1.5em !important;
        }

        .p-horarios {
            font-size: 1.1em;
            font-style: normal;
        }

        .p-destino {
            font-weight: normal !important;
        }
    </style>
    @endif

    <div class="background" id="background"></div>
    <div id="horarios">
        <?php
        $index = 0;
        ?>
        @foreach($destinations as $destination)
        @if( count($destination->horarios) > 0)
        <div class="row {{ ((is_int($index / 2))) ? 'destination-2' : 'destination'}}">
            <div class="col-2 text-center no-padding">
                <div class="place-name">
                    <p class="p-destino">
                        {{ $destination->name }}
                    </p>
                </div>
            </div>
            <div class="col-1 no-padding">
                <div class="place-price">
                    <p class="p-precio">
                        $ {{ number_format((int) $destination->amount) }}
                    </p>
                </div>
            </div>
            <div class="col-9 no-padding">
                <div class="place-horarios">
                    <p class="p-horarios">
                        @foreach($destination->horarios as $horario)
                        <span class="destination-hour">{{ $horario }}</span>
                        @endforeach
                    </p>
                </div>
            </div>
            <?php $index++ ?>
        </div>
        @endif
        @endforeach
    </div>

</body>

</html>