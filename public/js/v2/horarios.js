$(document).ready(function () {
    //URL del proyecto
    let url = $('#url').val();

    //Claves para nodos de lugar/pantalla
    let horarios = $('#horarios');
    let clave_lugar = $('#clave_lugar').val().toLowerCase();
    let clave_pantalla = $('#clave_pantalla').val();
    let saltos = 1;
    let tiempo = 10;

    // Clonamos 10 veces la vista
    for (i = 0; i < 20; i++) {
        horarios.clone().appendTo('body');
    }

    //Hace el autoscroll cada 10 milisegundos y vuelve a llamar la función (Recursiva)
    function pageScroll() {
        window.scrollBy(0, saltos);
        scrolldelay = setTimeout(pageScroll, tiempo);
    }

    //Genera la primera llamada a la función de autoscroll
    pageScroll();

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 5000) {
            horarios.clone().appendTo('body');
        }
    });

    //Configuración de Firebase
    var firebaseConfig = {
        apiKey: "AIzaSyDSI3_WJqCTzlhCez4O-El-5z2L2kd6p0w",
        authDomain: "flecha-amarilla-44ee8.firebaseapp.com",
        databaseURL: "https://flecha-amarilla-44ee8.firebaseio.com",
        projectId: "flecha-amarilla-44ee8",
        storageBucket: "flecha-amarilla-44ee8.appspot.com",
        messagingSenderId: "68164021952",
        appId: "1:68164021952:web:f14fb4eefd73eeac7a4725",
        measurementId: "G-3DT2S4T97W"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    let database = firebase.database();

    let clave_firebase = `pantallas/${clave_lugar}/${clave_pantalla}/`;

    //Valores a cambiar para apartado de destino
    let clave_destino = `${clave_firebase}destino/`;
    let destino_familia = database.ref(`${clave_destino}familia`);
    let destino_tamanio = database.ref(`${clave_destino}tamanio`);
    let destino_estilo = database.ref(`${clave_destino}estilo`);
    let destino_color = database.ref(`${clave_destino}color`);
    let destino_fondo = database.ref(`${clave_destino}fondo`);
    let destino_peso = database.ref(`${clave_destino}peso`);
    let destino_columnas = database.ref(`${clave_destino}columnas`);

    //Valores a cambiar para apartado de precio
    let clave_precio = `${clave_firebase}precio/`;
    let precio_familia = database.ref(`${clave_precio}familia`);
    let precio_tamanio = database.ref(`${clave_precio}tamanio`);
    let precio_estilo = database.ref(`${clave_precio}estilo`);
    let precio_color = database.ref(`${clave_precio}color`);
    let precio_fondo = database.ref(`${clave_precio}fondo`);
    let precio_peso = database.ref(`${clave_precio}peso`);
    let precio_columnas = database.ref(`${clave_precio}columnas`);

    //Valores a cambiar para apartado de horarios
    let clave_horarios = `${clave_firebase}horarios/`;
    let horarios_familia = database.ref(`${clave_horarios}familia`);
    let horarios_tamanio = database.ref(`${clave_horarios}tamanio`);
    let horarios_estilo = database.ref(`${clave_horarios}estilo`);
    let horarios_color = database.ref(`${clave_horarios}color`);
    let horarios_peso = database.ref(`${clave_horarios}peso`);
    let horario_columnas = database.ref(`${clave_horarios}columnas`);

    //Valores a cambiar para apartado general
    let fondo_destino_1 = database.ref(`${clave_firebase}fondo_destino_1`);
    let fondo_destino_2 = database.ref(`${clave_firebase}fondo_destino_2`);
    let escala_x = database.ref(`${clave_firebase}escala_x`);
    let escala_y = database.ref(`${clave_firebase}escala_y`);
    let saltos_velocidad = database.ref(`${clave_firebase}saltos_velocidad`);
    let tiempo_velocidad = database.ref(`${clave_firebase}tiempo_velocidad`);
    let color_linea_1 = database.ref(`${clave_firebase}color_linea_1`);
    let color_linea_2 = database.ref(`${clave_firebase}color_linea_2`);
    let separacion_lineas = database.ref(`${clave_firebase}separacion_lineas`);

    //Cambios para destino
    destino_familia.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-destino').css('font-family', snapshot.val());
            }
        }
    });

    destino_tamanio.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-destino').css('font-size', `${snapshot.val()}em`);
            }
        }
    });

    destino_estilo.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-destino').css('font-style', snapshot.val());
            }
        }
    });

    destino_color.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-destino').css('color', snapshot.val());
            }
        }
    });

    destino_fondo.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.place-name').css('background-color', snapshot.val());
            }
        }
    });

    destino_peso.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-destino').css('font-weight', snapshot.val());
            }
        }
    });

    destino_columnas.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                let parent = $('.p-destino').parent().parent();
                removeColClasses(parent);
                parent.addClass(`col-${snapshot.val()}`);
            }
        }
    });

    //Cambios para precio
    precio_familia.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-precio').css('font-family', snapshot.val());
            }
        }
    });

    precio_tamanio.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-precio').css('font-size', `${snapshot.val()}em`);
            }
        }
    });

    precio_estilo.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-precio').css('font-style', snapshot.val());
            }
        }
    });

    precio_color.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-precio').css('color', snapshot.val());
            }
        }
    });

    precio_fondo.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.place-price').css('background-color', snapshot.val());
            }
        }
    });

    precio_peso.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-precio').css('font-weight', snapshot.val());
            }
        }
    });

    precio_columnas.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                let parent = $('.p-precio').parent().parent();
                removeColClasses(parent);
                parent.addClass(`col-${snapshot.val()}`);
            }
        }
    });

    //Cambios para horarios
    horarios_familia.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-horarios').css('font-family', snapshot.val());
            }
        }
    });

    horarios_tamanio.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-horarios').css('font-size', `${snapshot.val()}em`);
            }
        }
    });

    horarios_estilo.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.destination-hour').css('font-style', snapshot.val());
            }
        }
    });

    horarios_color.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.destination-hour').css('color', snapshot.val());
            }
        }
    });

    horarios_peso.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.p-horarios').css('font-weight', snapshot.val());
            }
        }
    });

    horario_columnas.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                let parent = $('.p-horarios').parent().parent();
                removeColClasses(parent);
                parent.addClass(`col-${snapshot.val()}`);
            }
        }
    });

    //Cambios para información general
    fondo_destino_1.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.destination').css('background', snapshot.val());
            }
        }
    });

    fondo_destino_2.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.destination-2').css('background', snapshot.val());
            }
        }
    });

    escala_x.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('body').css('transform', `scaleX(${snapshot.val()})`);
            }
        }
    });

    escala_y.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('body').css('transform', `scaleY(${snapshot.val()})`);
            }
        }
    });

    saltos_velocidad.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                saltos = snapshot.val();
            }
        }
    });

    tiempo_velocidad.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                tiempo = snapshot.val();
            }
        }
    });

    color_linea_1.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.destination').css('border-top-color', snapshot.val());
            }
        }
    });

    color_linea_2.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('.destination-2').css('border-top-color', snapshot.val());
            }
        }
    });

    separacion_lineas.on('value', function (snapshot) {
        if (isConnected()) {
            if (snapshot.val() != null) {
                $('p').css('line-height', snapshot.val());
            }
        }
    });

    //Elimina las clases de "col-x" para permitir cambiarlas por otras
    function removeColClasses(element) {
        element.removeClass(`col-1`);
        element.removeClass(`col-2`);
        element.removeClass(`col-3`);
        element.removeClass(`col-4`);
        element.removeClass(`col-5`);
        element.removeClass(`col-6`);
        element.removeClass(`col-7`);
        element.removeClass(`col-8`);
        element.removeClass(`col-9`);
        element.removeClass(`col-10`);
        element.removeClass(`col-11`);
        element.removeClass(`col-12`);
    }

    async function isConnected() {
        try {
            const response = await checkConnection();

            if (response) {
                return response.connected;
            } else {
                return false;
            }

        } catch (err) {
            return false;
        }
    }

    function checkConnection() {
        return $.ajax({
            url: `${url}/v2/utils/checkconnection`,
            type: 'get'
        });
    }

});