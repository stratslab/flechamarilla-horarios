$(document).ready(function() {
    let horarios = $('#horarios');

    //Hace el autoscroll cada 10 milisegundos y vuelve a llamar la función (Recursiva)
    function pageScroll() {
        window.scrollBy(0, 5);
        scrolldelay = setTimeout(pageScroll, 1);
    }

    //Genera la primera llamada a la función de autoscroll
    pageScroll();

    //Valida si la página ha llegado a su final 
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            /*clearTimeout(scrolldelay);

            scrollTo(0, function () {
                pageScroll();
            });*/
            horarios.clone().appendTo('body');
        }
    });

});